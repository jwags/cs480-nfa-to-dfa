/**********************
Written by: Jordan Wagner
Last updated on: 10/26/2016
Written for: Professor William Confer
Written at: SUNY Polytechnic Institute
Written in: C Language
Used: GCC compiler
gcc version 4.8.5 (FreeBSD Ports Collection)

Purpose: Take in two files. 
1) The first file must be an file containing the details for an NFA.
-Line 1: Number of states, start at 0
-Line 2: alphabet (includes epsilon) (lowercase letters)
-Line 3: initial state
-line 4: Final states, separated by space
-line 5-end: the transitions for each state for each alphabet, and epsilon
i.e.
==============
|4           |
|ab          |
|0           |
|0 3         |
|1,2 -1 3    |
|-1 3 2      |
|1 -1 0,1    |
|-1 1 -1     |
==============
-Also, -1 means no transition

2) The second is a file of strings separated by new lines

The program will take in the NFA file, convert it to the DFA, then run each string
in the second file against our new DFA. The program will print "good" followed by
the string if the string lands on a final state, or "fail" followed by the string
if the string lands on a non-final state.
**********************/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

void ConvertAndCompare(char ***filenameone);//, char ****filenametwo);
void load_file(char *filename, char **buffer, size_t *size);


typedef struct State State;
struct State {
	char *begin;
	char *end;
};


int main(int argc, char *argv[]) {
	argc--;
	argv++;

	if (argc <= 1) {
		//print_usage();
		exit(EXIT_FAILURE);
	}

	//size_t size;
	//char *buffer = NULL;
	/*char ***fileOne = &argv;
	argc--;
	argv++;

	if (argc < 1) {
		//print_usage();
		exit(EXIT_FAILURE);
	}
	char ***fileTwo = &argv;*/
	ConvertAndCompare(&argv);//, &fileTwo);

}

int PowerOf(int num, int power) {
	int total = num;
	int i = 0;
	if (power == 0) {
		return 1;
	}
	else if (power == 1 && num != 0) {
		return num;
	}
	else if (power == 1 && num == 0) {
		return 0;
	}
	for (i = 1; i<power; i++) {
		total = total*num;
	}
	return total;
}

//Used to get number of states and initial state
int ConvertCtoN(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	char *NumberOfStates;
	int NumberOfStatesAsInt = 0;
	int i = 0;
	size_t length;
	/*
	* Skip whitespace, if any, at the beginning of the token.
	*/
	is_whitespace = true;
	while (is_whitespace) {
		switch (*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	end = begin;
	while (*end != '\n' && *end != '\r' && *end != '\0') {
		if (!(*end >= '0' && *end <= '9')) {
			return -1;
		}
		end++;
	}
	if (end == begin) {
		return -1;
	}
	length = (end - 1) - begin + 1;
	NumberOfStates = (char *)calloc(length + 1, sizeof(char));
	memcpy(NumberOfStates, begin, length);
	for (i = 1; i <= length; i++) {
		NumberOfStatesAsInt += (NumberOfStates[length - i] - '0')*(PowerOf(10, i - 1));
	}
	*buffer = end + 1 + 1;

	return NumberOfStatesAsInt;
}

char *GetAlphabet(char **buffer, int *SizeOfAlphabet) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	char *Alphabet;
	int i = 0;
	size_t length;
	/*
	* Skip whitespace, if any, at the beginning of the token.
	*/
	is_whitespace = true;
	while (is_whitespace) {
		switch (*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}



	end = begin;
	while (*end != '\n' && *end != '\r' && *end != '\0') {
		if (!(*end >= 'a' && *end <= 'z')) {
			printf("Invalid alphabet. Make sure the second line in your NFA file contains only lowercase letters.\n");
			exit(EXIT_FAILURE);
		}
		else {
			*SizeOfAlphabet = *SizeOfAlphabet + 1;
		}
		end++;
	}
	if (end == begin) {
		printf("Invalid alphabet. Make sure the first line in your NFA file contains only lowercase letters.\n");
		exit(EXIT_FAILURE);
	}
	length = end - begin;
	Alphabet = (char *)calloc(length + 1, sizeof(char));
	memcpy(Alphabet, begin, length);

	*buffer = end + 1 + 1;

	return Alphabet;
}

int *GetFinalStates(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	int *FinalStates;
	int i = 0, j = 0;
	int LengthOfCurrentFinalState = 0;
	int SavedCurrentState = false;
	int TotalFinalStates = 0;
	char *CurrentState;
	/*
	* Skip whitespace, if any, at the beginning of the token.
	*/
	is_whitespace = true;
	while (is_whitespace) {
		switch (*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	//Find number of final states. Used later.
	end = begin;
	while (*end != '\n' && *end != '\r' && *end != '\0') {
		if (!(*end >= '0' && *end <= '9') && *end != ' ') {
			printf("Invalid final state. Make sure the fourth line in your NFA file contains only numbers, which may be separated by spaces to indicate different states.\n");
			exit(EXIT_FAILURE);
		}

		if (*end == ' ' || *end == ',') {
			SavedCurrentState = false;
		}
		if (*end != ' ' && !SavedCurrentState) {
			SavedCurrentState = true;
			TotalFinalStates++;
		}
		end++;
	}

	if (end == begin) {
		printf("Invalid final state. Make sure the fourth line in your NFA file contains only numbers, which may be separated by spaces to indicate different states.\n");
		exit(EXIT_FAILURE);
	}


	//Store final states.
	FinalStates = (int *)calloc(TotalFinalStates+1, sizeof(int));
	FinalStates[0] = TotalFinalStates;
	end = begin;
	i = 1;
	while (i<(TotalFinalStates + 1)) {
		FinalStates[i] = 0;

		if (*end != ' ' && *end != '\n' && *end != '\r' && *end != '\0') {
			LengthOfCurrentFinalState++;
		}



		if ((*end == ' ' || *end == '\n' || *end == '\r' || *end == '\0') && LengthOfCurrentFinalState != 0) {
			CurrentState = (char *)calloc(LengthOfCurrentFinalState, sizeof(char));
			memcpy(CurrentState, (end - LengthOfCurrentFinalState), LengthOfCurrentFinalState);

			for (j = 1; j <= LengthOfCurrentFinalState; j++) {
				FinalStates[i] += (CurrentState[LengthOfCurrentFinalState - j] - '0')*(PowerOf(10, j - 1));
			}
			LengthOfCurrentFinalState = 0;
			i++;
		}

		end++;
	}

	*buffer = end + 1;

	return FinalStates;
}

int *GetTransitions(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	int *FinalStates;
	int i = 0, j = 0;
	int LengthOfCurrentState = 0;
	int SavedCurrentState = false;
	int TotalStates = 0;
	char *CurrentState;

	is_whitespace = true;
	while (is_whitespace) {
		switch (*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
		case '\n':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	end = begin;

	while (*end != '\n' && *end != '\r' && *end != '\0' && *end != ' ') {
		if (!(*end >= '0' && *end <= '9') && *end != ',' && (*end == '-' && *(end + 1) != '1')) {
			printf("Invalid state. Make sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
			exit(EXIT_FAILURE);
		}

		if (*end == ',') {
			SavedCurrentState = false;
		}
		if (*end != ',' && !SavedCurrentState) {
			if (*end == '-' && *(end + 1) == '1') {
				end++;
			}
			else {
				TotalStates++;
			}
			SavedCurrentState = true;
		}
		end++;
	}


	if (end == begin) {
		printf("Invalid state. Make sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
		exit(EXIT_FAILURE);
	}
	end = begin;
	int *Transitions;
	i = 1;
	// This was difficult. my logic in the following while loop to detect -1 was not working. I decided to
	//add this if instead. Now it works but I am too afraid to remove the logic  in the if incase I break stuff.
	if (TotalStates == 0) {
		Transitions = (int *)calloc(2, sizeof(int));
		Transitions[0] = 0;
		Transitions[1] = -1;
		end++; end++; end++;
	}
	else {
		Transitions = (int *)calloc(TotalStates + 1, sizeof(int));
		Transitions[0] = TotalStates;
		while (i<(TotalStates + 1)) {
			Transitions[i] = 0;

			if (*end != ',' && *end != ' ' && *end != '\n' && *end != '\r' && *end != '\0' && (*end != '-' && *(end + 1) != '1')) {
				LengthOfCurrentState++;
			}



			if (*end == ',' || *end == ' ' || *end == '\n' || *end == '\r' || *end == '\0' || (*end == '-' && *(end + 1) == '1')) {
				if (LengthOfCurrentState == 0) {

					if (TotalStates != 1) {
						printf("Invalid state. A if a transition for a state description contains -1, then it cannot contain anything else.\nMake sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
						exit(EXIT_FAILURE);
					}
					Transitions[i] = -1;
					end++;
				}
				else {
					CurrentState = (char *)calloc(LengthOfCurrentState, sizeof(char));
					memcpy(CurrentState, (end - LengthOfCurrentState), LengthOfCurrentState);


					for (j = 1; j <= LengthOfCurrentState; j++) {
						Transitions[i] = Transitions[i] + (CurrentState[LengthOfCurrentState - j] - '0')*(PowerOf(10, j - 1));
					}
					LengthOfCurrentState = 0;
				}
				i++;
			}
			end++;
		}
	}
	*buffer = end;

	return Transitions;
}

void GetEpsilonTransitions(char **buffer, int NumberOfStates, int SizeOfAlphabet, int ***StateTransitionTable) {
	SizeOfAlphabet = SizeOfAlphabet - 1;
	int i, j, k, l;
	bool dirty = true;
	bool isInitialRunThrough = true;
	bool isTransitionFound = false;
	while (dirty) {
		dirty = false;
		for (i = 0; i<NumberOfStates; i++) {
			if (isInitialRunThrough) {
				dirty = true;
				if (StateTransitionTable[i][SizeOfAlphabet][0] == 0) {
					StateTransitionTable[i][SizeOfAlphabet][0] = 1;
					StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0] + 1)*sizeof(int));
					StateTransitionTable[i][SizeOfAlphabet][1] = i; //Make sure current state is in epsilon. LOL this is my only documentation for this block of code.
				}
				else {
					for (l = 1; (l<(StateTransitionTable[i][SizeOfAlphabet][0] + 1) && !isTransitionFound); l++) {
						if (i == StateTransitionTable[i][SizeOfAlphabet][l]) {
							isTransitionFound = true;
						}
					}
					if (!isTransitionFound) {
						StateTransitionTable[i][SizeOfAlphabet][0] = StateTransitionTable[i][SizeOfAlphabet][0] + 1;
						StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0] + 1)*sizeof(int));
						StateTransitionTable[i][SizeOfAlphabet][StateTransitionTable[i][SizeOfAlphabet][0]] = i;
					}
					isTransitionFound = false;
				}
			}


			for (j = 1; j<(StateTransitionTable[i][SizeOfAlphabet][0] + 1); j++) {
				if ((isInitialRunThrough &&  StateTransitionTable[i][SizeOfAlphabet][j]<i) || !isInitialRunThrough) {
					for (k = 1; k<(StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][0] + 1); k++) {
						for (l = 1; (l<(StateTransitionTable[i][SizeOfAlphabet][0] + 1) && !isTransitionFound); l++) {
							if (StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][k] == StateTransitionTable[i][SizeOfAlphabet][l]) {
								isTransitionFound = true;
							}
						}
						if (!isTransitionFound) {
							dirty = true;
							StateTransitionTable[i][SizeOfAlphabet][0] = StateTransitionTable[i][SizeOfAlphabet][0] + 1;
							StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0] + 1)*sizeof(int));
							StateTransitionTable[i][SizeOfAlphabet][StateTransitionTable[i][SizeOfAlphabet][0]] = StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][k];
						}
						isTransitionFound = false;
					}
				}
			}
		}

		//use to print out epsilon on each run-through
		//============================================
		//for(j=0;j<NumberOfStates;j++) {
		//	printf("[%d]{ ",j);
		//	for(k=0;k<=StateTransitionTable[j][SizeOfAlphabet][0];k++) {
		//		printf("%d, ",StateTransitionTable[j][SizeOfAlphabet][k]);
		//	}
		//	printf("},\n");
		//}
		//===================

		isInitialRunThrough = false;
	}
}

int ***CreateNewStates(char **buffer, int NumberOfStates, int SizeOfAlphabet, int ***StateTransitionTable, int *NumberOfNewStatesIn, int InitialState, int *FinalStates) {
	int CurrentState = 0;
	int NumberOfNewStates = 1;
	SizeOfAlphabet = SizeOfAlphabet - 1;
	int i, j, k, l, m, n;
	bool isTransitionFound = false;
	bool isNewStateAlreadyFound = false;
	int MatchedNumbersForNewState = 0;
	int ***TempNewStates;
	bool isCurrentStateFound = false;
	bool isAFinalState = false;
	/*
	[0]{2|1,2} =>Our new state 0 is pointed to by 2 states: 1 and 3
	[1]{1|4} => Our new state 1 is pointed to by 1 state: 4
	[2]{1|2,4} => ...
	*/
	int **RelatedStatesTable = (int **)malloc(sizeof(int *));//[1][2];// [NewName] { NumberOfRelatedOriginalStates, OrigStateName1, OrigStateName2, ... }
	//RelatedStatesTable[0] = (int *)malloc(2 * sizeof(int));
	RelatedStatesTable[0] = (int *)malloc((StateTransitionTable[InitialState][SizeOfAlphabet][0]+1) * sizeof(int));
	RelatedStatesTable[0][0] = StateTransitionTable[InitialState][SizeOfAlphabet][0];
	for(i=1;i<(StateTransitionTable[InitialState][SizeOfAlphabet][0]+1);i++) {
		RelatedStatesTable[0][i] = StateTransitionTable[InitialState][SizeOfAlphabet][i];
	}
	//RelatedStatesTable[0][0] = 1;//these two lines says state 0 has 1 related state, and it is 0
	//RelatedStatesTable[0][1] = 0;//this is always true b/c when making new states, we start at state 0


	int ***NewStates;
	NewStates = (int ***)malloc(sizeof(int**) * 1);
	NewStates[0] = (int **)malloc(SizeOfAlphabet*sizeof(int*));
	for (i = 0; i<SizeOfAlphabet; i++) {
		NewStates[0][i] = (int*)malloc(sizeof(int)*1);
		NewStates[0][i][0] = 0;
	}


	while (CurrentState < NumberOfNewStates) {
		for (i = 1; i<(RelatedStatesTable[CurrentState][0] + 1); i++) {
			for (j = 0; j<SizeOfAlphabet; j++) {
				for (k = 1; k<(StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][0] + 1); k++) {
					for (l = 1; (l<(NewStates[CurrentState][j][0] + 1) && !isTransitionFound); l++) {
						if (NewStates[CurrentState][j][l] == StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k]) {
							isTransitionFound = true;
						}//ouch, my brain hurts.
					}
					if (!isTransitionFound && StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k] != -1) {
						NewStates[CurrentState][j][0] = NewStates[CurrentState][j][0] + 1;
						NewStates[CurrentState][j] = realloc(NewStates[CurrentState][j], (NewStates[CurrentState][j][0] + 1)*sizeof(int));
						NewStates[CurrentState][j][NewStates[CurrentState][j][0]] = StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k];
					}
					isTransitionFound = false;
				}
				for (k = 1; k<(StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][0] + 1); k++) {
					for (m = 1; m<(StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][0] + 1); m++) {
						for (l = 1; (l<(NewStates[CurrentState][j][0] + 1) && !isTransitionFound); l++) {
							if (NewStates[CurrentState][j][l] == StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m]) {
								isTransitionFound = true;
							}//ouch, my brain hurts.
						}
						if (!isTransitionFound && StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m] != -1) {
							NewStates[CurrentState][j][0] = NewStates[CurrentState][j][0] + 1;
							NewStates[CurrentState][j] = realloc(NewStates[CurrentState][j], (NewStates[CurrentState][j][0] + 1)*sizeof(int));
							NewStates[CurrentState][j][NewStates[CurrentState][j][0]] = StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m];
						}
						isTransitionFound = false;
					}

				}
			}
		}
		for (j = 0; j<SizeOfAlphabet; j++) {
			isNewStateAlreadyFound = false;
			for (l = 0; l<NumberOfNewStates; l++) {
				if (!isNewStateAlreadyFound) {
					MatchedNumbersForNewState = 0;
					for (n = 1; n<(NewStates[CurrentState][j][0] + 1); n++) {
						if (!isNewStateAlreadyFound) {
							for (m = 1; m<(RelatedStatesTable[l][0] + 1); m++) {
								if (NewStates[CurrentState][j][n] == RelatedStatesTable[l][m]) {
									MatchedNumbersForNewState++;
								}
							}
						}
					}
					if (MatchedNumbersForNewState == RelatedStatesTable[l][0] && MatchedNumbersForNewState == NewStates[CurrentState][j][0]) {
						isNewStateAlreadyFound = true;
					}

				}
			}
			if (!isNewStateAlreadyFound && NewStates[CurrentState][j][0] != 0) {
				NumberOfNewStates++;
				RelatedStatesTable = (int **)realloc(RelatedStatesTable, NumberOfNewStates*sizeof(int *));
				RelatedStatesTable[NumberOfNewStates - 1] = (int *)malloc(sizeof(int));
				RelatedStatesTable[NumberOfNewStates - 1][0] = 0;
				NewStates = (int ***)realloc(NewStates, NumberOfNewStates*sizeof(int **));
				NewStates[NumberOfNewStates-1] = (int**)malloc(SizeOfAlphabet*sizeof(int*));
				for (m = 0; m<SizeOfAlphabet; m++) {
					NewStates[NumberOfNewStates - 1][m] = (int*)malloc(sizeof(int)*1);
					NewStates[NumberOfNewStates - 1][m][0] = 0;
				}
				l = NumberOfNewStates;
				for (n = 1; n<(NewStates[CurrentState][j][0] + 1); n++) {
					RelatedStatesTable[NumberOfNewStates - 1][0] = RelatedStatesTable[NumberOfNewStates - 1][0] + 1;
					RelatedStatesTable[NumberOfNewStates - 1] = realloc(RelatedStatesTable[NumberOfNewStates - 1], (RelatedStatesTable[NumberOfNewStates - 1][0] + 1)*sizeof(int));
					RelatedStatesTable[NumberOfNewStates - 1][RelatedStatesTable[NumberOfNewStates - 1][0]] = NewStates[CurrentState][j][n];
				}
			}
			isNewStateAlreadyFound = false;
		}
		CurrentState++;
	}

	MatchedNumbersForNewState = 0;
	for(i=0;i<NumberOfNewStates;i++) {
		for(j=0;j<SizeOfAlphabet;j++) {
			for(l=0;l<NumberOfNewStates && !isCurrentStateFound;l++) {
				for(k=1;k<(NewStates[i][j][0]+1) && !isCurrentStateFound;k++) {
					for(m=1;m<(RelatedStatesTable[l][0]+1);m++) {
						if(RelatedStatesTable[l][m]==NewStates[i][j][k]) {
							MatchedNumbersForNewState++;
						}
					}
				}
				//I think this is wrong. have to 
				if(MatchedNumbersForNewState==RelatedStatesTable[l][0] && MatchedNumbersForNewState==NewStates[i][j][0]) {
					NewStates[i][j]= (int*)malloc(2*sizeof(int));
					NewStates[i][j][1] = l;
					isCurrentStateFound = true;
				}
				MatchedNumbersForNewState = 0;
			}
			isCurrentStateFound = false;
		}
		for(j=0;j<(RelatedStatesTable[i][0]+1) && !isAFinalState;j++) {
			for(k=1;k<(FinalStates[0]+1) && !isAFinalState;k++) {
				if(RelatedStatesTable[i][j]==FinalStates[k]) {
					isAFinalState = true;
				}
			}
		}
		for(j=0;j<SizeOfAlphabet;j++) {
			printf("well this is %d\n",(isAFinalState? 1 : 0));
			NewStates[i][j][0] = (isAFinalState? 1 : 0);
		}
		isAFinalState = false;
	}
	
	//Use to print out new states table
	/*printf("============================\n\nNew states look like:\n\taE*\tbE*\n");
	for (i = 0; i<NumberOfNewStates; i++) {
		//printf("here1\n");
		printf("[%d]{\t", i);
		for (j = 0; j<SizeOfAlphabet; j++) {
			//printf("here2\n");
			printf("[%d]{", NewStates[i][j][0]);
			for (k = 1; k<2;k++) {//(NewStates[i][j][0] + 1); k++) {
				printf("%d, ", NewStates[i][j][k]);
			}
			printf("}\t");
		}
		printf("\n");
	}*/
	*NumberOfNewStatesIn = NumberOfNewStates;
	return NewStates;
}

void CheckForValidStrings(char **buffer, int ***NewStates, int SizeOfAlphabet, int NumberOfNewStates, int InitialState, char *Alphabet) {
	int i,j,k;
	char *begin = *buffer;
	char *tempBegin = NULL;
	char *end = NULL;
	end = begin;
	int CurrentState;
	//Used to print out new states table
	printf("============================\n\nNew states look like:\n\taE*\tbE*\n");
	for (i = 0; i<NumberOfNewStates; i++) {
		printf("[%d]{\t", i);
		for (j = 0; j<(SizeOfAlphabet-1); j++) {
			printf("[%d]{", NewStates[i][j][0]);
			for (k = 0; k<2;k++) {
				printf("%d, ", NewStates[i][j][k]);
			}
			printf("}\t");
		}
		printf("\n");
	}
	int string = 1;
	bool isCurrentCharacterInAlphabet = false;
	bool isStringBad = false;
	while(*end != '\0') {
		begin=end;
		if(*end=='E') {
			if(NewStates[InitialState][i][0]==1) {
				printf("good E\n");
			} else {
				printf("fail E\n");
			}
			end++;
		} else {
			while(*end != '\0' && *end != '\n' && *end != '\r' && !isStringBad) {
				for(i=0;i<(SizeOfAlphabet-1) && !isCurrentCharacterInAlphabet;i++) {
					if(*end == Alphabet[i]) {
						isCurrentCharacterInAlphabet = true;
					}
				}
				if(!isCurrentCharacterInAlphabet) {
					isStringBad = true;
				}
				isCurrentCharacterInAlphabet = false;
				end++;
			}
			if(isStringBad) {
				//printf("The alphabet for string %d is NOT valid.\n", string);
				printf("fail ");
				while(begin<end) {
					printf("%c",*begin);
					begin++;
				}
				printf("\n");
			} else {
				tempBegin=begin;
				//printf("The alphabet for string %d is valid.\n", string);
				CurrentState = InitialState;
				while(begin<end) {
					for(i=0;i<(SizeOfAlphabet-1) && *begin != Alphabet[i];i++) {}
					CurrentState = NewStates[CurrentState][i][1];
					begin++;
				}
				if(NewStates[CurrentState][i][0]==1) { 
					//printf("String %d lands on a final state.\n", string);
					printf("good ");
				} else {
					//printf("String %d does NOT land on a final state.\n", string);
					printf("fail ");
				}
				while(tempBegin<end) {
					printf("%c",*tempBegin);
					tempBegin++;
				}
				printf("\n");
			}
		}
		//printf("====================\n");
		string++;
		isStringBad = false;
		if(*end != '\0') {
			end++;
			if(*end != '\0') {
				end++;
			}
		}
	}
}

void ConvertAndCompare(char ***filenameone) {
	size_t size;
	char *buffer = NULL;
	load_file(**filenameone, &buffer, &size);
	char *b;  /* editable copy of the buffer pointer */
	b = buffer;
	int i, j, k;
	int **currentTransitions;
	int *TempTransitions;

	int NumberOfStates = 0;
	NumberOfStates = ConvertCtoN(&b);
	if (NumberOfStates == -1) {
		printf("Invalid number of states. Make sure the first line in your NFA file contains only digits\n");
		exit(EXIT_FAILURE);
	}

	char *Alphabet;
	int SizeOfAlphabet = 0;
	Alphabet = GetAlphabet(&b, &SizeOfAlphabet);
	SizeOfAlphabet++; //Accounting for epsilon


	int InitialState = ConvertCtoN(&b);
	if (InitialState == -1) {
		printf("Invalid initial state. Make sure the third line in your NFA file contains only digits\n");
		exit(EXIT_FAILURE);
	}

	int *FinalStates;
	FinalStates = GetFinalStates(&b);

	int *Transitions;
	int ***StateTransitionTable;
	StateTransitionTable = (int***)malloc(sizeof(int**)*NumberOfStates);


	for (i = 0; i<NumberOfStates; i++) {
		StateTransitionTable[i] = (int**)malloc(sizeof(int*)*SizeOfAlphabet);
		for (j = 0; j<SizeOfAlphabet; j++) {
			Transitions = GetTransitions(&b);
			StateTransitionTable[i][j] = malloc(sizeof(int) * ((*Transitions) + 1));
			for (k = 0; k <= *Transitions; k++) {
				StateTransitionTable[i][j][k] = Transitions[k];

			}
		}
	}
	GetEpsilonTransitions(&b, NumberOfStates, SizeOfAlphabet, StateTransitionTable);

	//Used to print out states table with the epsilon thingy
	/*printf("\n\n==========================\n\n\n\n\n");
	for (i = 0; i<NumberOfStates; i++) {
		printf("[%d]{\n", i);
		for (j = 0; j<SizeOfAlphabet; j++) {
			printf("\t[%d]{\n\t\t", j);
			if (StateTransitionTable[i][j][0] == 0) {
				printf("No Transitions");
			}
			else {
				for (k = 0; k <= StateTransitionTable[i][j][0]; k++) {
					printf("%d, ", StateTransitionTable[i][j][k]);
				}
			}
			printf("},\n");
		}
		printf("},\n");
	}*/

	int NumberOfNewStates;
	int ***NewStates;
	NewStates = (int ***)malloc(sizeof(int**)*1);
	NewStates[0] = (int **)malloc(SizeOfAlphabet*sizeof(int*));

	NewStates = CreateNewStates(&b, NumberOfStates, SizeOfAlphabet, StateTransitionTable, &NumberOfNewStates, InitialState, FinalStates);
	
	//Used to print out new states table
	/*printf("============================\n\nNew states look like:\n\taE*\tbE*\n");
	for (i = 0; i<NumberOfNewStates; i++) {
		//printf("here1\n");
		printf("[%d]{\t", i);
		for (j = 0; j<(SizeOfAlphabet-1); j++) {
			//printf("here2\n");
			printf("[%d]{", NewStates[i][j][0]);
			for (k = 1; k<2;k++) {//(NewStates[i][j][0] + 1); k++) {
				printf("%d, ", NewStates[i][j][k]);
			}
			printf("}\t");
		}
		printf("\n");
	}*/
	(*filenameone)++;
	load_file(**filenameone, &buffer, &size);
	b = buffer;
	CheckForValidStrings(&b, NewStates, SizeOfAlphabet, NumberOfNewStates, InitialState, Alphabet);
}



//Taken from William Confers program for Program 1 homework. CS480.
void load_file(char *filename, char **buffer, size_t *size) {
	long tell_size;
	FILE *file = NULL;
	file = fopen(filename, "rb");
	if (!file) {
		fprintf(stderr, "Could not open \"%s\"", filename);
		perror("");
		return;
	}

	if (fseek(file, 0, SEEK_END) != 0) {
		fprintf(stderr, "Could not seek to end of \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}

	if ((tell_size = ftell(file)) == EOF) {
		fprintf(stderr, "Could not tell file position in \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}
	rewind(file);
	*size = (size_t)tell_size;

	*buffer = (char *)malloc(sizeof(char) * (*size + 1));
	if (*buffer == NULL) {
		fprintf(stderr, "Could not allocate file buffer for \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}

	(*buffer)[*size] = '\0'; /* the NULL terminator is how we'll detect EOF */

	if (fread(*buffer, sizeof(char), *size, file) != *size) {
		if (feof(file)) {
			fprintf(stderr, "Premature EOF in \"%s\"\n", filename);
		}
		else {
			fprintf(stderr, "Error reading \"%s\"\n", filename);
			perror("");
		}
		free(*buffer);
		*buffer = NULL;
	}

cleanup_file:
	fclose(file);

	return;
}