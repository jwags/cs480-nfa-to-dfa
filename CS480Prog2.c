
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

void ConvertNFAtoDFA(char ***filename);
void load_file(char *filename, char **buffer, size_t *size);


typedef struct State State;
struct State {
	char *begin;
	char *end;
};


int main(int argc, char *argv[]) {
	argc--;
	argv++;

	if(argc <= 1) {
		//print_usage();
		exit(EXIT_FAILURE);
	}

	//size_t size;
	//char *buffer = NULL;
	ConvertNFAtoDFA(&argv);

}

int PowerOf(int num, int power) {
	int total = num;
	int i = 0;
	if(power==0) {
		return 1;
	}  else if(power==1 && num!=0) {
		return num;
	} else if(power==1 && num==0) {
		return 0;
	}
	for(i=1;i<power;i++) {
		total = total*num;
	}
	return total;
}

//Used to get number of states and initial state
int ConvertCtoN(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	char *NumberOfStates;
	int NumberOfStatesAsInt = 0;
	int i =0;
	size_t length;
	/*
	 * Skip whitespace, if any, at the beginning of the token.
	 */
	is_whitespace = true;
	while(is_whitespace) {
		switch(*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	end = begin;
	while(*end!='\n' && *end!='\r' && *end!='\0') {
		if(!(*end >= '0' && *end <= '9')) {
			return -1;
		}
		end++;
	}
	if(end==begin) {
		return -1;
	}
	length = (end-1) - begin + 1;
	NumberOfStates = (char *)calloc(length + 1, sizeof(char));
	memcpy(NumberOfStates, begin, length);
	for(i=1;i<=length;i++) {
		NumberOfStatesAsInt += (NumberOfStates[length-i]-'0')*(PowerOf(10, i-1));
	}
	*buffer = end + 1 + 1;

	return NumberOfStatesAsInt;
}

char *GetAlphabet(char **buffer, int *SizeOfAlphabet) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	char *Alphabet;
	int i =0;
	size_t length;
	/*
	 * Skip whitespace, if any, at the beginning of the token.
	 */
	is_whitespace = true;
	while(is_whitespace) {
		switch(*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	

	end = begin;
	while(*end!='\n' && *end!='\r' && *end!='\0') {
		if(!(*end >= 'a' && *end <= 'z')) {
			printf("Invalid alphabet. Make sure the second line in your NFA file contains only lowercase letters.\n");
			exit(EXIT_FAILURE);
		} else {
			*SizeOfAlphabet = *SizeOfAlphabet + 1;
		}
		end++;
	}
	if(end==begin) {
		printf("Invalid alphabet. Make sure the first line in your NFA file contains only lowercase letters.\n");
		exit(EXIT_FAILURE);
	}
	length = end - begin;
	Alphabet = (char *)calloc(length + 1, sizeof(char));
	memcpy(Alphabet, begin, length);

	*buffer = end + 1 + 1;

	return Alphabet;
}

int *GetFinalStates(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	int *FinalStates;
	int i = 0, j = 0;
	int LengthOfCurrentFinalState=0;
	int SavedCurrentState=false;
	int TotalFinalStates=0;
	char *CurrentState;
	/*
	 * Skip whitespace, if any, at the beginning of the token.
	 */
	is_whitespace = true;
	while(is_whitespace) {
		switch(*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	//Find number of final states. Used later.
	end = begin;
	while(*end!='\n' && *end!='\r' && *end!='\0') {
		if(!(*end >= '0' && *end <= '9') && *end != ' ') {
			printf("Invalid final state. Make sure the fourth line in your NFA file contains only numbers, which may be separated by spaces to indicate different states.\n");
			exit(EXIT_FAILURE);
		}

		if(*end == ' ' || *end == ',') {
			SavedCurrentState = false;
		}
		if(*end != ' ' && !SavedCurrentState) {
			SavedCurrentState = true;
			TotalFinalStates++;
		}
		end++;
	}

	if(end==begin) {
		printf("Invalid final state. Make sure the fourth line in your NFA file contains only numbers, which may be separated by spaces to indicate different states.\n");
		exit(EXIT_FAILURE);
	}
	

	//Store final states.
	FinalStates = (int *)calloc(TotalFinalStates, sizeof(int));
	end = begin;
	i=0;
	while(i<TotalFinalStates) {
		FinalStates[i] = 0;

		if(*end != ' ' && *end != '\n' && *end != '\r' && *end != '\0') {
			LengthOfCurrentFinalState++;
		} 
	
			

		if((*end == ' ' || *end=='\n' || *end=='\r' || *end=='\0') && LengthOfCurrentFinalState!=0) {
			CurrentState = (char *)calloc(LengthOfCurrentFinalState, sizeof(char));
			memcpy(CurrentState, (end-LengthOfCurrentFinalState), LengthOfCurrentFinalState);

			for(j=1;j<=LengthOfCurrentFinalState;j++) {
				FinalStates[i] += (CurrentState[LengthOfCurrentFinalState-j]-'0')*(PowerOf(10, j-1));
			}
			LengthOfCurrentFinalState = 0;
			i++;
		}

		end++;
	}

	*buffer = end + 1;

	return FinalStates;
}

int *GetTransitions(char **buffer) {
	char *begin = *buffer; // starting character of token
	char *end = NULL;      // end character of the token
	bool is_whitespace;
	int *FinalStates;
	int i = 0, j = 0;
	int LengthOfCurrentState=0;
	int SavedCurrentState=false;
	int TotalStates=0;
	char *CurrentState;

	is_whitespace = true;
	while(is_whitespace) {
		switch(*begin) {
		case ' ':
		case '\t':
		case '\b':
		case '\f':
		case '\v':
		case '\n':
			begin++;
			break;
		default:
			is_whitespace = false;
			break;
		}
	}

	end = begin;

	while(*end!='\n' && *end!='\r' && *end!='\0' && *end != ' ') {
		if(!(*end >= '0' && *end <= '9') && *end != ',' && (*end == '-' && *(end+1) != '1')) {
			printf("1Invalid state. Make sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
			exit(EXIT_FAILURE);
		}

		if(*end == ',') {
			SavedCurrentState = false;
		}
		if(*end != ',' && !SavedCurrentState) {
			if(*end=='-' && *(end+1) == '1') {
				end++;
			} else {
				TotalStates++;
			}
			SavedCurrentState = true;
		}
		end++;
	}


	if(end==begin) {
		printf("Invalid state. Make sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
		exit(EXIT_FAILURE);
	}
	end = begin;
	int *Transitions;
	i=1;
	// This was difficult. my logic in the following while loop to detect -1 was not working. I decided to
	//add this if instead. Now it works but I am too afraid to remove the logic  in the if incase I break stuff.
	if(TotalStates==0) { 
		Transitions = (int *)calloc(2, sizeof(int));
		Transitions[0] = 0;
		Transitions[1] = -1;
		end++;end++;end++;
	} else {
		Transitions = (int *)calloc(TotalStates+1, sizeof(int));
		Transitions[0] = TotalStates;
		while(i<(TotalStates+1)) {
			Transitions[i] = 0;

			if(*end != ',' && *end != ' ' && *end != '\n' && *end != '\r' && *end != '\0' && (*end != '-' && *(end+1) != '1')) {
				LengthOfCurrentState++;
			} 
		
				

			if(*end == ',' || *end==' ' || *end=='\n' || *end=='\r' || *end=='\0' || (*end == '-' && *(end+1) == '1')) {
				if(LengthOfCurrentState==0) {
					
					if(TotalStates!=1) {
						printf("Invalid state. A if a transition for a state description contains -1, then it cannot contain anything else.\nMake sure the line 6 and beyond in your NFA file contains only numbers, which may be separated by spaces to indicate different states, commas (,) to indicate multiple states, or -1 to indicate no transition.\n");
						exit(EXIT_FAILURE);
					}
					Transitions[i] = -1;
					end++;
				} else {
					CurrentState = (char *)calloc(LengthOfCurrentState, sizeof(char));
					memcpy(CurrentState, (end-LengthOfCurrentState), LengthOfCurrentState);

						
					for(j=1;j<=LengthOfCurrentState;j++) {
						Transitions[i] = Transitions[i] + (CurrentState[LengthOfCurrentState-j]-'0')*(PowerOf(10, j-1));
					}
					LengthOfCurrentState = 0;
				}
				i++;
			}
			end++;
		}
	}
	*buffer = end;

	return Transitions;
}

void GetEpsilonTransitions(char **buffer, int NumberOfStates, int SizeOfAlphabet, int *StateTransitionTable[NumberOfStates][SizeOfAlphabet]) {
	SizeOfAlphabet = SizeOfAlphabet - 1;
	int i, j, k, l;
	bool dirty = true;
	bool isInitialRunThrough = true;
	bool isTransitionFound = false;
	while(dirty) {
		dirty = false;
		for(i=0;i<NumberOfStates;i++) {
			if(isInitialRunThrough) {
				dirty = true;
				if(StateTransitionTable[i][SizeOfAlphabet][0]==0) {
					StateTransitionTable[i][SizeOfAlphabet][0] = 1;
					StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0]+1)*sizeof(int));
					StateTransitionTable[i][SizeOfAlphabet][1] = i; //Make sure current state is in epsilon. LOL this is my only documentation for this block of code.
				} else {
					for(l=1;(l<(StateTransitionTable[i][SizeOfAlphabet][0]+1) && !isTransitionFound);l++) {
						if(i == StateTransitionTable[i][SizeOfAlphabet][l]) {
							isTransitionFound = true;
						}
					}
					if(!isTransitionFound) {
						StateTransitionTable[i][SizeOfAlphabet][0] = StateTransitionTable[i][SizeOfAlphabet][0] + 1;
						StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0]+1)*sizeof(int));
						StateTransitionTable[i][SizeOfAlphabet][StateTransitionTable[i][SizeOfAlphabet][0]] = i;
					}
					isTransitionFound = false;
				}
			}
	
	
			for(j=1;j<(StateTransitionTable[i][SizeOfAlphabet][0]+1);j++) {
				if((isInitialRunThrough &&  StateTransitionTable[i][SizeOfAlphabet][j]<i) || !isInitialRunThrough) {
					for(k=1;k<(StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][0]+1);k++) {
						for(l=1;(l<(StateTransitionTable[i][SizeOfAlphabet][0]+1) && !isTransitionFound);l++) {
							if(StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][k] == StateTransitionTable[i][SizeOfAlphabet][l]) {
								isTransitionFound = true;
							}
						}
						if(!isTransitionFound) {
							dirty = true;
							StateTransitionTable[i][SizeOfAlphabet][0] = StateTransitionTable[i][SizeOfAlphabet][0] + 1;
							StateTransitionTable[i][SizeOfAlphabet] = realloc(StateTransitionTable[i][SizeOfAlphabet], (StateTransitionTable[i][SizeOfAlphabet][0]+1)*sizeof(int));
							StateTransitionTable[i][SizeOfAlphabet][StateTransitionTable[i][SizeOfAlphabet][0]] = StateTransitionTable[StateTransitionTable[i][SizeOfAlphabet][j]][SizeOfAlphabet][k];
						}
						isTransitionFound = false;
					}
				}
			}
		}

		//use to print out epsilon on each run-through
		//============================================
		//for(j=0;j<NumberOfStates;j++) {
		//	printf("[%d]{ ",j);
		//	for(k=0;k<=StateTransitionTable[j][SizeOfAlphabet][0];k++) {
		//		printf("%d, ",StateTransitionTable[j][SizeOfAlphabet][k]);
		//	}
		//	printf("},\n");
		//}
		//===================

		isInitialRunThrough = false;
	}
}

void CreateNewStates(char **buffer, int NumberOfStates, int SizeOfAlphabet, int *StateTransitionTable[NumberOfStates][SizeOfAlphabet], int ***NewStates) {//}[1][SizeOfAlphabet]) {
	//well on line 526 i discovered that i created 220 new states. perhaps there is an error? lol...
	int CurrentState = 0;
	int NumberOfNewStates = 1;
	SizeOfAlphabet = SizeOfAlphabet - 1;
	int i, j, k, l, m, n;
	bool isTransitionFound = false;
	bool isNewStateAlreadyFound = false;
	int MatchedNumbersForNewState = 0;
	int ***TempNewStates;
	/*
	[0]{2|1,2} =>Our new state 0 is pointed to by 2 states: 1 and 3
	[1]{1|4} => Our new state 1 is pointed to by 1 state: 4
	[2]{1|2,4} => ...
	*/
	int **RelatedStatesTable = (int **)malloc(sizeof(int *));//[1][2];// [NewName] { NumberOfRelatedOriginalStates, OrigStateName1, OrigStateName2, ... }
	RelatedStatesTable[0] = (int *)malloc(2*sizeof(int));
	RelatedStatesTable[0][0] = 1;//these two lines says state 0 has 1 related state, and it is 0
	RelatedStatesTable[0][1] = 0;//this is always true b/c when making new states, we start at state 0
	
	//Is this incorrect? YES - Defining new state 0 has 0 related states for every alphabet character as of now
	for(i=0;i<SizeOfAlphabet;i++) {
		//==========================
		//==========================
		NewStates[0][i] = realloc(NewStates[0][i], sizeof(int));
		//NewStates[0][i] = (int*)malloc(sizeof(int));
		//==========================
		NewStates[0][i][0] = 0;
	}
	
	
	while(CurrentState < NumberOfNewStates) {
		//printf("here2\n");
		for(i=1;i<(RelatedStatesTable[CurrentState][0]+1);i++) {//might change this to StateTransitionTable[CurrentState]
			
			for(j=0;j<SizeOfAlphabet;j++) {
				//printf("here3=%d\n",j);
				if(CurrentState==1) {
					printf("aaaaaaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh!!\n");
					//scanf("%d",k);
				}
				//printf("here4 where j=%d\n",j);
				//printf("error here. This=%d\n",RelatedStatesTable[CurrentState][i]);
				//make transitions for current character in alphabet sync with NewStatesTable
				for(k=1;k<(StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][0]+1);k++) {// runs through character
					//printf("here16=%d=%d\n",CurrentState,j);
					for(l=1;(l<(NewStates[CurrentState][j][0]+1) && !isTransitionFound);l++) { //check if transition exists for current alphabet character in current new state
						//printf("here17\n");
						if(NewStates[CurrentState][j][l] == StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k]) {//StateTransitionTable[i][SizeOfAlphabet][k]) {
							isTransitionFound = true;
						}//ouch, my brain hurts.
					}//initially no transitions exists for any character in alphabet so previous loop should not run on first go through
					if(!isTransitionFound && StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k] != -1) {
						printf("size of currentnewstate=%d\n",NewStates[CurrentState][j][0]);
						NewStates[CurrentState][j][0] = NewStates[CurrentState][j][0] + 1;
						printf("size of currentnewstate=%d\n",NewStates[CurrentState][j][0]);
							printf("currentstate=%d\n", CurrentState);
							printf("j=%d\n", j);
						//printf("here18=%d\n",NewStates[CurrentState][j][0]);
						//printf("here19=%d=%d\n", i, RelatedStatesTable[CurrentState][i]);
						//================================
						//================================
						NewStates[CurrentState][j] = realloc(NewStates[CurrentState][j], (NewStates[CurrentState][j][0]+1)*sizeof(int));
						//NewStates[CurrentState][j] = (int*)malloc((NewStates[CurrentState][j][0]+1)*sizeof(int));
						//================================
						//printf("here20=%d=%d\n",NewStates[CurrentState][j][0],NewStates[CurrentState][j][NewStates[CurrentState][j][0]]);
						NewStates[CurrentState][j][NewStates[CurrentState][j][0]] = StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k];
						//printf("here31=%d\n",NewStates[CurrentState][j][0]);//NewStates[CurrentState][j][0]]);
						//isTransitionFound = false;
						
						
					}//not sure => StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k]
					isTransitionFound = false;
				}
				//printf("here21\n");
				for(k=1;k<(StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][0]+1);k++) {//runs through epsilon
					//printf("here22=%d\n", StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][0]);
					//StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k]
					for(m=1;m<(StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][0]+1);m++) {
						
						for(l=1;(l<(NewStates[CurrentState][j][0]+1) && !isTransitionFound);l++) { //check if transition exists for current alphabet character in current new state
							//printf("here23: ?%d==%d?\n", NewStates[CurrentState][j][l], StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m]);
							if(NewStates[CurrentState][j][l] == StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m]) {//StateTransitionTable[i][SizeOfAlphabet][k]) {
								isTransitionFound = true;
							}//ouch, my brain hurts.
							//printf("Transition found=%d\n",isTransitionFound);
						}
						//printf("here24=%d\n",isTransitionFound);//StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m]);
						if(!isTransitionFound && StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m] != -1) {//StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k] != -1) {
							//printf("here331=%d\n",NewStates[CurrentState][j][0]);
							//if(NumberOfNewStates>1) { printf("here451=%d\n",RelatedStatesTable[1][1]); }
							printf("size of currentnewstate=%d\n",NewStates[CurrentState][j][0]);
							NewStates[CurrentState][j][0] = NewStates[CurrentState][j][0] + 1;
							printf("size of currentnewstate=%d\n",NewStates[CurrentState][j][0]);
							printf("1currentstate=%d\n", CurrentState);
							printf("j=%d\n", j);
							//printf("here25=%d\n",NewStates[CurrentState][j][0]);
							//if(NumberOfNewStates>1) { printf("here452=%d\n",RelatedStatesTable[1][1]); }
							//RelatedStatesTable = realloc(RelatedStatesTable, sizeof(RelatedStatesTable)*sizeof(int));
							//============================
							//============================
							NewStates[CurrentState][j] = realloc(NewStates[CurrentState][j], (NewStates[CurrentState][j][0]+1)*sizeof(int));
							//NewStates[CurrentState][j] = (int*)malloc((NewStates[CurrentState][j][0]+1)*sizeof(int));
							//============================
							NewStates[CurrentState][j][NewStates[CurrentState][j][0]] = StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m];//StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k];
							
							//printf("here453=%d=%d\n",StateTransitionTable[StateTransitionTable[RelatedStatesTable[CurrentState][i]][SizeOfAlphabet][k]][j][m],NewStates[CurrentState][j][NewStates[CurrentState][j][0]]);
							//printf("here41=%d\n",NewStates[CurrentState][j][NewStates[CurrentState][j][0]]);
							//if(NumberOfNewStates>1) { printf("here40=%d\n",RelatedStatesTable[1][1]); if(RelatedStatesTable[1][1]==3){exit(1);}}
							//exit(1);
							//isTransitionFound = false;
						}//not sure => StateTransitionTable[RelatedStatesTable[CurrentState][i]][j][k]
						isTransitionFound = false;
					}
					
				}
			}
			//printf("here5=%d\n",NewStates[CurrentState][1][NewStates[CurrentState][1][0]]);
		}
		//printf("here6=%d\n",NewStates[CurrentState][1][NewStates[CurrentState][1][0]]);
			
				//check for new states, if so, add it to related states table
			for(j=0;j<SizeOfAlphabet;j++) {
				//printf("here7\n");
				isNewStateAlreadyFound = false;
				//for(n=1;n<(NewStates[CurrentState][j][0]+1);n++) {
				for(l=0;l<NumberOfNewStates;l++) {
					//printf("over=> %d<%d\n", n, NewStates[CurrentState][j][0]);
					//printf("here8\n");
					if(!isNewStateAlreadyFound) {
						//for(l=0;l<NumberOfNewStates;l++) {
						MatchedNumbersForNewState = 0;
						for(n=1;n<(NewStates[CurrentState][j][0]+1);n++) {
							//printf("here9\n");
							if(!isNewStateAlreadyFound) {
								//MatchedNumbersForNewState = 0;
								for(m=1;m<(RelatedStatesTable[l][0]+1);m++) {
									//printf("here10\n");
									if(NewStates[CurrentState][j][n] == RelatedStatesTable[l][m]) {
										MatchedNumbersForNewState++;
									}
								}
								/*if(MatchedNumbersForNewState != RelatedStatesTable[l][0] || MatchedNumbersForNewState != NewStates[CurrentState][j][0]) {
									printf("here11\n");
									//Have to check count for both to prevent adding partial matches
									//Found a new state. Add it to RelatedStatesTable
									NumberOfNewStates++;
									if(NumberOfNewStates>10) {
										exit(1);
									}
									printf("here12\n");
									RelatedStatesTable = (int **)realloc(RelatedStatesTable, NumberOfNewStates*sizeof(int *));
									printf("here13\n");
									RelatedStatesTable[NumberOfNewStates-1] = (int *)malloc(sizeof(int));
									RelatedStatesTable[NumberOfNewStates-1][0] = 0;
									printf("here14=%d\n",RelatedStatesTable[NumberOfNewStates-1][0]);
									NewStates = (int ***)realloc(NewStates, NumberOfNewStates*sizeof(int **));
									printf("here15\n");
									NewStates[NumberOfNewStates-1] = (int **)realloc(NewStates[NumberOfNewStates-1], SizeOfAlphabet*sizeof(int *));
									printf("here33\n");
									for(m=0;m<SizeOfAlphabet;m++) {
										printf("here34=%d\n",NumberOfNewStates);
										NewStates[NumberOfNewStates-1][m] = realloc(NewStates[NumberOfNewStates-1][m], sizeof(int));
										printf("here35\n");
										NewStates[NumberOfNewStates-1][m][0] = 0;
										printf("here36\n");
									}
									isNewStateAlreadyFound = true;
									l=NumberOfNewStates;
									
								}*/
							}
						}
						if(MatchedNumbersForNewState == RelatedStatesTable[l][0] && MatchedNumbersForNewState == NewStates[CurrentState][j][0]) {
							isNewStateAlreadyFound = true;
						}
						
					}
					
					
					/*if(isNewStateAlreadyFound) {
						RelatedStatesTable[NumberOfNewStates-1][0] = RelatedStatesTable[NumberOfNewStates-1][0] + 1;
						printf("here26=%d, then=%d\n",CurrentState, NumberOfNewStates);
						RelatedStatesTable[NumberOfNewStates-1] = realloc(RelatedStatesTable[NumberOfNewStates-1], (RelatedStatesTable[NumberOfNewStates-1][0]+1)*sizeof(int));
						printf("here27=%d\n",RelatedStatesTable[NumberOfNewStates-1][0]);
						RelatedStatesTable[NumberOfNewStates-1][RelatedStatesTable[NumberOfNewStates-1][0]] = NewStates[CurrentState][j][n];
						printf("here28=%d, here29=%d, here30=%d, here32=%d\n", NewStates[CurrentState][j][0], j, n, NewStates[CurrentState][j][n]);
						n=NewStates[CurrentState][j][0]+1;
					}*/
				}
				if(!isNewStateAlreadyFound) {//MatchedNumbersForNewState != RelatedStatesTable[l][0] || MatchedNumbersForNewState != NewStates[CurrentState][j][0]) {
					//printf("here11\n");
					//Have to check count for both to prevent adding partial matches
					//Found a new state. Add it to RelatedStatesTable
					NumberOfNewStates++;
					/*if(NumberOfNewStates>10) {
						exit(1);
					}*/
					//printf("here12\n");
					//==========================
					//==========================
					RelatedStatesTable = (int **)realloc(RelatedStatesTable, NumberOfNewStates*sizeof(int *));
					//RelatedStatesTable = (int **)malloc(NumberOfNewStates*sizeof(int *));
					//==========================
					//printf("here13\n");
					RelatedStatesTable[NumberOfNewStates-1] = (int *)malloc(sizeof(int));
					RelatedStatesTable[NumberOfNewStates-1][0] = 0;
					//printf("here14=%d\n",RelatedStatesTable[NumberOfNewStates-1][0]);
					//==========================
					//==========================
					NewStates = (int ***)realloc(NewStates, NumberOfNewStates*sizeof(int **));
					//NewStates = (int ***)malloc(NumberOfNewStates*sizeof(int**));
					//==========================
					//printf("here15\n");
					//==========================
					//==========================
					NewStates[NumberOfNewStates-1] = (int **)realloc(NewStates[NumberOfNewStates-1], SizeOfAlphabet*sizeof(int *));
					//NewStates[NumberOfNewStates-1] = (int**)malloc(SizeOfAlphabet*sizeof(int*));
					//==========================
					//printf("here33\n");
					//printf("here66=%d\n",NewStates[CurrentState][1][NewStates[CurrentState][1][0]]);
					for(m=0;m<SizeOfAlphabet;m++) {
						printf("here34=%d\n",NumberOfNewStates);
						//============================
						//============================
						NewStates[NumberOfNewStates-1][m] = realloc(NewStates[NumberOfNewStates-1][m], sizeof(int));
						//NewStates[NumberOfNewStates-1][m] = (int*)malloc(sizeof(int));
						//============================

					printf("here67=%d\n",NewStates[CurrentState][1][NewStates[CurrentState][1][0]]);
						printf("here35\n");
						NewStates[NumberOfNewStates-1][m][0] = 0;
					printf("here68=%d\n",NewStates[CurrentState][1][NewStates[CurrentState][1][0]]);
						printf("here36\n");
					}
					//isNewStateAlreadyFound = true;
					l=NumberOfNewStates;




					for(n=1;n<(NewStates[CurrentState][j][0]+1);n++) {
						RelatedStatesTable[NumberOfNewStates-1][0] = RelatedStatesTable[NumberOfNewStates-1][0] + 1;
						printf("here26=%d, then=%d\n",CurrentState, NumberOfNewStates);
						//============================
						//============================
						RelatedStatesTable[NumberOfNewStates-1] = realloc(RelatedStatesTable[NumberOfNewStates-1], (RelatedStatesTable[NumberOfNewStates-1][0]+1)*sizeof(int));
						//RelatedStatesTable[NumberOfNewStates-1] = (int*)malloc((RelatedStatesTable[NumberOfNewStates-1][0]+1)*sizeof(int));
						//============================
						printf("here27=%d\n",RelatedStatesTable[NumberOfNewStates-1][0]);
						RelatedStatesTable[NumberOfNewStates-1][RelatedStatesTable[NumberOfNewStates-1][0]] = NewStates[CurrentState][j][n];
						printf("here28=%d, here29=%d, here30=%d, here32=%d\n", NewStates[CurrentState][j][0], j, n, NewStates[CurrentState][j][n]);
						//n=NewStates[CurrentState][j][0]+1;
					}
					if(NumberOfNewStates>0) { printf("here401=%d", RelatedStatesTable[1][1]); }

printf("here69=%d\n",NewStates[CurrentState][1][0]);

					printf("============================\n\nNew states look like:\n\taE*\tbE*\n");
					for(m=0;m<NumberOfNewStates;m++) {
						//printf("here1\n");
						printf("[%d]{\t",m);
						for(n=0;n<SizeOfAlphabet;n++) {
							//printf("here2\n");
							printf("[%d]{",NewStates[m][n][0]);
							for(l=1;l<(NewStates[m][n][0]+1);l++) {
								printf("%d, ",NewStates[m][n][l]);
							}
							printf("}\t");
						}
						printf("\n");
					}
					//exit(1);




				}
				isNewStateAlreadyFound = false;

			}
		
		
		//StateTransitionTable[CurrentState]
		printf("increasing current state\n");
		CurrentState++;
	}
	
	printf("============================\n\nNew states look like:\n\taE*\tbE*\n");
	for(i=0;i<NumberOfNewStates;i++) {
		//printf("here1\n");
		printf("[%d]{\t",i);
		for(j=0;j<SizeOfAlphabet;j++) {
			//printf("here2\n");
			printf("[%d]{",NewStates[i][j][0]);
			for(k=1;k<(NewStates[i][j][0]+1);k++) {
				printf("%d, ",NewStates[i][j][k]);
			}
			printf("}\t");
		}
		printf("\n");
	}
}

void ConvertNFAtoDFA(char ***filename) {

	size_t size;
	char *buffer = NULL;
	load_file(**filename, &buffer, &size);
	char *b;  /* editable copy of the buffer pointer */
	b = buffer;
	int i,j,k;
	
	int **currentTransitions;
	int *TempTransitions;

	int NumberOfStates = 0;
	NumberOfStates = ConvertCtoN(&b);
	if(NumberOfStates==-1) {
		printf("Invalid number of states. Make sure the first line in your NFA file contains only digits\n");
		exit(EXIT_FAILURE);
	}

	char *Alphabet;
	int SizeOfAlphabet = 0;
	Alphabet = GetAlphabet(&b, &SizeOfAlphabet);
	SizeOfAlphabet++; //Accounting for epsilon


	int InitialState = ConvertCtoN(&b);
	if(InitialState==-1) {
		printf("Invalid initial state. Make sure the third line in your NFA file contains only digits\n");
		exit(EXIT_FAILURE);
	}

	int *FinalStates;
	FinalStates = GetFinalStates(&b);

	int *Transitions;
	int *StateTransitionTable[NumberOfStates][SizeOfAlphabet];

	for(i=0;i<NumberOfStates;i++) {
		for(j=0;j<SizeOfAlphabet;j++) {
			Transitions = GetTransitions(&b);
			StateTransitionTable[i][j] = malloc(sizeof(int) * ((*Transitions)+1));
			for ( k = 0; k <= *Transitions; k++ ) {
				StateTransitionTable[i][j][k] = Transitions[k];

			}
		}
	}
	GetEpsilonTransitions(&b, NumberOfStates, SizeOfAlphabet, StateTransitionTable);

	printf("\n\n==========================\n\n\n\n\n");
	for(i=0;i<NumberOfStates;i++) {
		printf("[%d]{\n",i);
		for(j=0;j<SizeOfAlphabet;j++) {
			printf("\t[%d]{\n\t\t",j);
			if(StateTransitionTable[i][j][0]==0) {
				printf("No Transitions");
			} else {
				for(k=0;k<=StateTransitionTable[i][j][0];k++) {
					printf("%d, ",StateTransitionTable[i][j][k]);
				}
			}
			printf("},\n");
		}
		printf("},\n");
	}

	int ***NewStates = (int ***)malloc(sizeof(int **));//[1][SizeOfAlphabet];
	NewStates[0] = (int **)malloc(SizeOfAlphabet*sizeof(int *));
	CreateNewStates(&b, NumberOfStates, SizeOfAlphabet, StateTransitionTable, NewStates);

}



//Taken from William Confers program for Program 1 homework. CS480.
void load_file(char *filename, char **buffer, size_t *size) {

	long tell_size;
	FILE *file = NULL;

	file = fopen(filename, "rb");
	if(!file) {
		fprintf(stderr, "Could not open \"%s\"", filename);
		perror("");
		return;
	}

	if(fseek(file, 0, SEEK_END) != 0) {
		fprintf(stderr, "Could not seek to end of \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}

	if((tell_size = ftell(file)) == EOF) {
		fprintf(stderr, "Could not tell file position in \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}
	rewind(file);
	*size = (size_t)tell_size;

	*buffer = (char *)malloc(sizeof(char) * (*size + 1));
	if(*buffer == NULL) {
		fprintf(stderr, "Could not allocate file buffer for \"%s\"\n", filename);
		perror("");
		goto cleanup_file;
	}

	(*buffer)[*size] = '\0'; /* the NULL terminator is how we'll detect EOF */

	if(fread(*buffer, sizeof(char), *size, file) != *size) {
		if(feof(file)) {
			fprintf(stderr, "Premature EOF in \"%s\"\n", filename);
		}
		else {
			fprintf(stderr, "Error reading \"%s\"\n", filename);
			perror("");
		}
		free(*buffer);
		*buffer = NULL;
	}

cleanup_file:
	fclose(file);

	return;
}